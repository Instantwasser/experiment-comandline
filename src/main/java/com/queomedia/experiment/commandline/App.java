package com.queomedia.experiment.commandline;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.ProcessBuilder.Redirect;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

/**
 * Hello world!
 *
 */

public class App {

    public static void writeFile(String fileText, String fileName) throws IOException {
        String str = fileText;
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            writer.write(str);
        }
    }

    public static String choose(List<String> options, String lastInputFile) throws IOException {

        do {
            
            Map<Integer, String> optionsMap = new TreeMap<>();
            for (int i = 0; i < options.size(); i++) {
                optionsMap.put(i + 1, options.get(i));
            }
            
            Path file = Paths.get(lastInputFile);
            if(Files.exists(file)) {                
                String lastGivenValue = Files.readString(file);
                optionsMap.put(0, lastGivenValue);
            }
            
            for(Entry<Integer, String> optionEntry : optionsMap.entrySet()) {
                System.out.println(optionEntry.getKey() + ". " + optionEntry.getValue());
            }
            
            System.out.println(" ");
            String input = System.console().readLine();

            boolean inputIsDirectory = input.length() > 5;
            if (inputIsDirectory) {
                writeFile(input, lastInputFile);
                return input;            
            } else {
                try {
                    int optionsKey = Integer.parseInt(input);
                    String value = optionsMap.get(optionsKey);
                    if (value != null ) {
                        writeFile(value, lastInputFile);
                        return value;
                    } //else, invalid option, loop will handle retry                    
                } catch (NumberFormatException e) {
                    //nothing to do, while loop will handle retry
                }
            }
        } while (true);
    }

    public static File chooseDirectory(List<String> directories) throws IOException {
        System.out.println();
        System.out.println("Choose directory... (pick a number or enter your path manually)");
        System.out.println();
        
        return new File(choose(directories, "lastDirectory.txt"));
    }

    public static String chooseImplement(List<String> implement) throws IOException {

        System.out.println();
        System.out.println("Choose project to implement...");
        System.out.println();

        return choose(implement, "lastImplement.txt");
    }

    public static String selectBranchName() throws IOException {

        String lastBranch = Files.readString(Paths.get("lastBranch.txt"));
        System.out.println(" ");
        System.out.println("Please enter Branch name");
        System.out.println("Last Branch name was: " + lastBranch);
        System.out.println("Press enter to apply last Branch name or enter Branch name manually");

        String branchName = System.console().readLine();
        if (branchName.length() > 3) {
            writeFile(branchName, "lastBranch.txt");
        } else {
            branchName = lastBranch;
        }

        return branchName;
    }

    public static String selectCherryPick() throws IOException {

        String defaultCherryPick = Files.readString(Paths.get("cherryPick.txt"));
        System.out.println(" ");
        System.out.println("Please enter cherrypick");
        System.out.println("default cherrypick is: " + defaultCherryPick);
        System.out.println("Press enter to apply last cherrypick or enter cherrypick manually");

        String cherryPick = System.console().readLine();

        if (cherryPick.length() >= 2) {
            writeFile(cherryPick, "cherryPick.txt");
        } else {
            cherryPick = defaultCherryPick;
        }
        return cherryPick;
    }

    public static List<String> buildCherrypickScript(String branchName, String cherryPick,
            String choosenImplementString) {

         // @formatter:off
         return Arrays.asList(
                "git remote add SourceForCherryPick " + choosenImplementString,
                "git fetch SourceForCherrypick master",
                "git pull",
                "git checkout -b " + branchName,
                "git cherry-pick " + cherryPick,
                "pause",
                "git push -u origin " + branchName + " --push-option merge_request.create --push-option merge_request.target=master",
                "git remote rm SourceForCherryPick");
         // @formatter:on
    }

    private static void executeScript(List<String> skriptCommands, File directory)
            throws InterruptedException, IOException {
        for (String command : skriptCommands) {
            if (command.equals("pause")) {
                executePauseCommand(); //to pause the console if there are problems with the cherry pick
            } else {
                executeCommand(command, directory);
            }
        }
    }

    public static void executeCommand(String command, File directory) throws InterruptedException, IOException {

        System.out.println();
        System.out.println(command);

        ProcessBuilder processBuilder = new ProcessBuilder();
        processBuilder.redirectOutput(Redirect.INHERIT);
        processBuilder.redirectError(Redirect.INHERIT);
        processBuilder.directory(directory);
        processBuilder.command("cmd.exe", "/c", command);

        Process process = processBuilder.start();
        process.waitFor(1, TimeUnit.MINUTES);
    }

    public static void executePauseCommand() {

        System.out.println(" ");
        System.out.println("Press Enter to continue...");
        System.console().readLine();
    }

    public static void main(String[] args) throws IOException, InterruptedException {

        do {

            List<String> directories = Files.readAllLines(Paths.get("directories.txt"));
            List<String> implement = Files.readAllLines(Paths.get("implements.txt"));

            File directory = chooseDirectory(directories);
            String choosenImplementString = chooseImplement(implement);
            String branchName = selectBranchName();
            String cherryPick = selectCherryPick();

            System.out.println();
            System.out.println("This is your script");
            System.out.println();

            List<String> script = buildCherrypickScript(branchName, cherryPick, choosenImplementString);
            for (String command : script) {
                System.out.println(command);
            }

            System.out.println();
            System.out.println("Is that ok?");
            System.out.println("[Y/J] yes     [N] no     [Q] quit");

            String userDecisionScript = System.console().readLine().strip();

            if (userDecisionScript.equalsIgnoreCase("y") || userDecisionScript.equalsIgnoreCase("j")) {
                executeScript(script, directory);

                System.out.println("****************");
                System.out.println("      done      ");
                System.out.println("****************");
            } else if (userDecisionScript.equalsIgnoreCase("n")) {

            } else if (userDecisionScript.equalsIgnoreCase("q")) {
                return;
            }

        } while (true);
    }

}
