Documentation
=============

Steps after creation

    cd existing_folder
    git init
    git remote add origin git@gitlab.com:Instantwasser/experiment-comandline.git
    git add .
    git commit -m "Initial commit"
    git push -u origin master
    
If https instead of ssl is used, then replace `git remote add origin git@gitlab.com...` by

    git remote add origin  https://gitlab.com/Instantwasser/experiment-comandline.git

If you use an different Git account with an different configuration

    git config user.name "Louis Hohaus"
    git config user.email "louishohaus@gmx.de"